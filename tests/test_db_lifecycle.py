import pytest
from icms_orm.orm_interface import OrmManager as Manager
from .conftest import TestParameters
from sqlalchemy.engine.reflection import Inspector
import icms_orm
import sqlalchemy as sa
from icms_orm.cmspeople import PersonHistory
from icms_orm.cmsanalysis import Analysis
from icms_orm.toolkit import Voting
from icms_orm.common import OrgUnit
from icms_orm.epr import TimeLineUser
from icms_orm.old_notes import Note
from icms_orm.orm_interface.declarative_bases_factory import IcmsDeclarativeBasesFactory


def schema_name_for_bind_key(bind_key):
    if bind_key == icms_orm.cms_people_bind_key(): return None
    return IcmsDeclarativeBasesFactory.get_default_schema_name_for_bind_key(bind_key)

@pytest.mark.parametrize('bind_key', TestParameters.bind_keys_dropwise())
def test_dropping_tables(db_manager, test_config, bind_key):
    Manager.drop_all(db_manager, bind=bind_key)
    db_engine = db_manager.get_engine_for_bind_key(bind_key)
    inspector = Inspector.from_engine(db_engine)
    tables = Inspector.get_table_names(inspector, schema=schema_name_for_bind_key(bind_key))
    views = Inspector.get_view_names(inspector, schema=schema_name_for_bind_key(bind_key))
    assert len(tables) == 0, 'Expected no tables on bind {1} but found: {0}'.format(tables, bind_key)
    assert len(views) == 0, 'Expected no views on bind {1} but found {0}'.format(views, bind_key)


@pytest.mark.parametrize('bind_key', TestParameters.bind_keys())
def test_creating_tables_and_views(db_manager, test_config, bind_key):
    """
    Attemps tables and views creation and checks the outcome.
    WARNING: baked-in non-zero expectation for views count under public and EPR schemas!
    """
    Manager.create_all(self=db_manager, bind=bind_key)
    db_engine = db_manager.get_engine_for_bind_key(bind_key)
    inspector = Inspector.from_engine(db_engine)
    assert len(Inspector.get_table_names(inspector, schema=schema_name_for_bind_key(bind_key))) > 0
    if bind_key in {icms_orm.epr_bind_key(), icms_orm.cms_common_bind_key()}:
        assert len(Inspector.get_view_names(inspector, schema=schema_name_for_bind_key(bind_key))) > 0


class TestOperationsOnMappedTypes():
    @pytest.mark.parametrize('class_object', [PersonHistory, Voting, OrgUnit, Analysis, TimeLineUser, Note])
    def test_session_uses_proper_bind_for_queries(self, db_manager, drop_create_all_databases, class_object):
        # A test like that would detect what seems like an anomaly but it's something hacked around actually.
        # proper_bind_key = class_object.__table__.info.get('bind_key')
        # proper_url = url.make_url(db_manager.config.get('SQLALCHEMY_BINDS').get(proper_bind_key))
        # session = class_object.session()
        # actual_url = session.bind.engine.url
        # assert proper_url == actual_url
        assert class_object.query.count() == 0
        assert db_manager.session.query(class_object).count() == 0

    @pytest.mark.parametrize('class_object', [PersonHistory, Voting, OrgUnit, Analysis, TimeLineUser, Note])
    def test_session_uses_proper_bind_for_subqueries(self, db_manager, drop_create_all_databases, class_object):
        q = class_object.session().query(*class_object.primary_keys())
        sq = q.subquery()
        assert db_manager.session().query(sq).count() == 0


    @pytest.mark.parametrize('class_object', [PersonHistory, Voting, OrgUnit, Analysis, TimeLineUser, Note])
    def test_session_uses_proper_bind_for_aliases(self, db_manager, drop_create_all_databases, class_object):
        class_alias = sa.orm.aliased(class_object)
        assert class_object.session().query(class_alias).count() == 0


    @pytest.mark.parametrize('mapped_type', [PersonHistory, Voting, OrgUnit, Analysis, TimeLineUser, Note])
    def test_inspection(self, db_manager, drop_create_all_databases, mapped_type):
        """
        A problem showed once that columns returned by inspection.inspect lost their connection to mother bind
        and so they would be queried for under the default bind and problems would arise.
        So our base class implements a methods for grabbing those PKs as a list of InstrumentedAttribute instances.
        """
        rows = mapped_type.session().query(*mapped_type.primary_keys()).all()
        assert len(rows) == 0, 'The database may be empty but the queries should work anyway'
