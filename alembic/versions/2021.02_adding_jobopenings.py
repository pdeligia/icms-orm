"""adding JobOpenings

Revision ID: 2021.02
Revises: 2021.01
Create Date: 2021-06-30 18:19:12.662076

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2021.02'
down_revision = '2021.01'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('job_opening',
    sa.Column('id', sa.SmallInteger(), autoincrement=True, nullable=False),
    sa.Column('title', sa.String(length=200), nullable=False),
    sa.Column('description', sa.String(length=1000), nullable=False),
    sa.Column('requirements', sa.String(length=1000), nullable=False),
    sa.Column('start_date', sa.Date(), nullable=False),
    sa.Column('end_date', sa.Date(), nullable=True),
    sa.Column('deadline', sa.Date(), nullable=False),
    sa.Column('position_id', sa.SmallInteger(), nullable=True),
    sa.Column('status', sa.String(length=80), nullable=False),
    sa.Column('last_modified', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['position_id'], ['public.position.id'], name=op.f('fk_job_opening_position_id_position'), onupdate='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_opening')),
    schema='public'
    )


def downgrade():
    op.drop_table('job_opening', schema='public')
