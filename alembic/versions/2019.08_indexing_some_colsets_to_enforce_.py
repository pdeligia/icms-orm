"""Indexing some colsets to enforce uniqueness

Revision ID: 2019.08
Revises: 2019.07
Create Date: 2019-09-20 15:13:21.174022

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2019.08'
down_revision = '2019.07'
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint('unit_type_domain_key', 'org_unit', ['type_id', 'domain'], schema='public')
    op.create_unique_constraint('position_name_unit_type_id', 'position', ['name', 'unit_type_id'], schema='public')


def downgrade():
    op.drop_constraint('position_name_unit_type_id', 'position', schema='public', type_='unique')
    op.drop_constraint('unit_type_domain_key', 'org_unit', schema='public', type_='unique')
