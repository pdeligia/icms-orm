"""adding nationality column to public.Person

Revision ID: 2019.09
Revises: 2019.08
Create Date: 2019-12-02 13:30:15.313855

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2019.09'
down_revision = '2019.08'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('person', sa.Column('nationality', sa.String(length=2), nullable=True))
    op.create_foreign_key(op.f('fk_person_nationality_country'), 'person', 'country', ['nationality'], ['code'], source_schema='public', referent_schema='public', onupdate='CASCADE', ondelete='SET NULL')


def downgrade():
    op.drop_constraint(op.f('fk_person_nationality_country'), 'person', schema='public', type_='foreignkey')
    op.drop_column('person', 'nationality')