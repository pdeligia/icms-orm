"""projects_and_assignments_in_common

Revision ID: 2020.01
Revises: 2019.09
Create Date: 2020-04-14 15:40:25.486959

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2020.01'
down_revision = '2019.09'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('project',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('code', sa.String(length=32), nullable=False),
        sa.Column('name', sa.String(length=64), nullable=False),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_project')),
        sa.UniqueConstraint('code', name=op.f('uq_project_code')),
        schema='public'
    )
    
    op.create_table('project_lifespan',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('code', sa.String(length=32), nullable=False),
        sa.Column('start_year', sa.Integer(), nullable=False),
        sa.Column('end_year', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['code'], ['public.project.code'], name=op.f('fk_project_lifespan_code_project')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_project_lifespan')),
        schema='public'
    )

    op.create_table('assignment',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('cms_id', sa.Integer(), nullable=False),
        sa.Column('project_code', sa.String(length=32), nullable=False),
        sa.Column('fraction', sa.Float(), nullable=False),
        sa.Column('start_date', sa.Date(), nullable=False),
        sa.Column('end_date', sa.Date(), nullable=True),
        sa.ForeignKeyConstraint(['cms_id'], ['public.person.cms_id'], name=op.f('fk_assignment_cms_id_person')),
        sa.ForeignKeyConstraint(['project_code'], ['public.project.code'], name=op.f('fk_assignment_project_code_project')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_assignment')),
    schema='public')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('assignment', schema='public')
    op.drop_table('project_lifespan', schema='public')
    op.drop_table('project', schema='public')
    # ### end Alembic commands ###
