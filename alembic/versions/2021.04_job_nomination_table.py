"""job nomination table

Revision ID: 2021.04
Revises: 2021.03
Create Date: 2021-09-08 15:55:20.798700

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2021.04'
down_revision = '2021.03'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('job_opening', 'position_id', existing_type=sa.SmallInteger(), existing_nullable=True,nullable=False)
    op.alter_column('job_opening', 'deadline', new_column_name='nominations_deadline', existing_type=sa.Date(), nullable=False)
    op.alter_column('job_opening', 'description', existing_type=sa.VARCHAR(length=1000),type_=sa.String(length=4000), nullable=False)
    op.alter_column('job_opening', 'requirements', existing_type=sa.VARCHAR(length=1000),type_=sa.String(length=4000), nullable=False)
    op.add_column('job_opening', sa.Column('days_for_nominee_response', sa.Integer(), nullable=True))
    op.add_column('job_opening', sa.Column('job_unit_id', sa.SmallInteger(), nullable=False))
    op.create_foreign_key(op.f('fk_job_opening_job_unit_id_org_unit'), 'job_opening', 'org_unit', ['job_unit_id'], ['id'],
     source_schema='public', referent_schema='public', onupdate='CASCADE')
    op.add_column('job_opening', sa.Column('comment', sa.String(length=1000), nullable=True))
    op.add_column('job_opening', sa.Column('nominee_no_reply', sa.Boolean(), nullable=True))
    op.add_column('job_opening', sa.Column('send_mail_to_nominee', sa.Boolean(), nullable=True))
    op.alter_column('job_opening', 'nominee_no_reply', comment='use to store the value("True(acceptance)", "False(rejection)" or "Null") when nominee does not reply within the deadline', nullable=True)
    
    op.create_table('job_nomination',
    sa.Column('nomination_id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('job_id', sa.Integer(), nullable=False),
    sa.Column('nominee_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['nominee_id'], [u'public.person.cms_id'], name=op.f('fk_job_nomination_nominee_id_person'), onupdate='CASCADE', ondelete='SET NULL'),
    sa.ForeignKeyConstraint(['job_id'], [u'public.job_opening.id'], name=op.f('fk_job_nomination_job_id_job_opening'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('nomination_id', name=op.f('pk_job_nomination')),
    schema='public'
    )
    op.create_table('job_nomination_status',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('actor_id', sa.Integer(), nullable=False),
    sa.Column('nomination_id', sa.Integer(), nullable=False),
    sa.Column('actor_remarks', sa.String(length=1000), nullable=True),
    sa.Column('status', sa.String(length=128), nullable=False),
    sa.Column('last_modified', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['nomination_id'], [u'public.job_nomination.nomination_id'], name=op.f('fk_job_nomination_status_nomination_id_job_nomination'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['actor_id'], [u'public.person.cms_id'], name=op.f('fk_job_nomination_status_actor_id_person'), onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_nomination_status')),
    schema='public'
    )


def downgrade():
    op.alter_column('job_opening', 'position_id', existing_type=sa.SmallInteger(), existing_nullable=False, nullable=True)
    op.alter_column('job_opening', 'nominations_deadline', new_column_name='deadline', existing_type=sa.Date(), nullable=False)
    op.alter_column('job_opening', 'description', existing_type=sa.String(length=4000),type_=sa.VARCHAR(length=1000), nullable=False)
    op.alter_column('job_opening', 'requirements', existing_type=sa.String(length=4000),type_=sa.VARCHAR(length=1000), nullable=False)
    op.drop_constraint(op.f('fk_job_opening_job_unit_id_org_unit'), 'job_opening', schema='public', type_='foreignkey')
    op.drop_column('job_opening', 'days_for_nominee_response')
    op.drop_column('job_opening', 'nominee_no_reply')
    op.drop_column('job_opening', 'job_unit_id')
    op.drop_column('job_opening', 'comment')
    op.drop_column('job_opening', 'send_mail_to_nominee')
    

    op.drop_table('job_nomination_status', schema='public')
    op.drop_table('job_nomination', schema='public')
