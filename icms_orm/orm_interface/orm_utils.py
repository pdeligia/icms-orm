from sqlalchemy.sql import util as sql_util
from sqlalchemy.orm.mapper import Mapper
from sqlalchemy.sql import ClauseElement
import logging


class BindKeyExtractor():
    KEY_OF_BIND_KEY = 'bind_key'
    KEY_OF_MAPPED_TABLE = 'mapped_table'
    KEY_OF_INFO_DICT = 'info'
    KEY_OF_PERSIST_SELECTABLE = 'persist_selectable'

    @classmethod
    def safely_extract_from_any(cls, *args):
        for arg in args:
            if arg is None:
                continue
            for _method in [cls.extract_from_clause, cls.extract_from_mapper]:
                try:
                    bk = _method(arg)
                    if bk:
                        return bk
                except Exception as _e:
                    logging.debug(_e)
        return None

    @classmethod
    def extract_from_clause(cls, clause) -> str:
        if clause is not None and isinstance(clause, ClauseElement):
            for t in sql_util.find_tables(clause, include_crud=True):
                _bk = cls._extract_from_info_owner(t)
                if _bk:
                    return _bk
        return None

    @classmethod
    def extract_from_mapper(cls, mapper) -> str:
        if mapper is not None and isinstance(mapper, Mapper):
            # apparently mapped table can be found in different places depending on sqlalchemy's version
            mapped_table = None
            if hasattr(mapper, cls.KEY_OF_PERSIST_SELECTABLE):
                mapped_table = getattr(mapper, cls.KEY_OF_PERSIST_SELECTABLE, None)
            elif hasattr(mapper, cls.KEY_OF_MAPPED_TABLE):
                mapped_table = mapped_table or getattr(mapper, cls.KEY_OF_MAPPED_TABLE, None)
            if mapped_table is not None:
                return cls._extract_from_info_owner(mapped_table)
        return None

    @classmethod
    def _extract_from_info_owner(cls, info_owner):
        if hasattr(info_owner, cls.KEY_OF_INFO_DICT):
            info = getattr(info_owner, cls.KEY_OF_INFO_DICT, {})
            if info:
                return info.get(cls.KEY_OF_BIND_KEY, None)
        return None
