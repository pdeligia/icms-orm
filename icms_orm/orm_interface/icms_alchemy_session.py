from icms_orm.orm_interface.orm_manager import OrmManager
from sqlalchemy.orm import Session
from icms_orm.orm_interface.orm_utils import BindKeyExtractor


class IcmsAlchemySession(Session):
    """
    Supplanting the standard SQLAlchemy session and emulating the SignallingSession of 
    Flask-SQLAlachemy but without the modifications tracking (for now at least).
    So only the binds selection is added (which anyway needed patching wrt Flask-SQLAlchemy)
    """

    def __init__(self, db: OrmManager, autocommit=False, autoflush=True, **options):
        self._db_manager = db
        bind = options.pop('bind', None) or db.get_engine()
        binds = options.pop('binds', self._get_db_binds_map())

        Session.__init__(
            self, autocommit=autocommit, autoflush=autoflush,
            bind=bind, binds=binds, **options
        )

    def get_bind(self, mapper=None, clause=None):
        bind_key = BindKeyExtractor.safely_extract_from_any(mapper, clause)
        if bind_key is not None:
            return self.get_db_manager().get_engine(bind=bind_key)
        return Session.get_bind(self, mapper=mapper, clause=clause)

    def get_db_manager(self):
        return self._db_manager

    def get_binds(self):
        """
        Proxy method for naming compatibility with flask-SQLAlchemy
        """
        return self._get_db_binds_map()

    def _get_db_binds_map(self):
        binds = dict()
        dbm = self.get_db_manager()
        for _key in dbm.get_bind_keys():
            tables = dbm.get_tables_for_bind_key(_key)
            debase = dbm.get_declarative_base_for_bind_key(_key)
            engine = dbm.get_engine_for_bind_key(_key)
            binds[debase] = engine
            for _t in tables:
                binds[_t] = engine
        return binds
