from sqlalchemy import types
import re


def c2k(ia):
    """
    Returns a reasonable text representation of provided InstrumentedAttribute instance.
    This can be used in scenarios where a group of columns is queried with intention to subsequently render them on
    the page with some standard, pre-defined description that can be associated with the return value of this function.
    :param ia: instance of InstrumentedAttribute, like Person.cmsId
    :return: a string like 'PeopleData.cmsId'
    """
    return '%s.%s' % (ia.parent.tables[0].key, ia.key)


def translate_bool(something):
    if isinstance(something, str):
        return something.lower() in ('yes', 'true', 'y', 't', '1')
    return bool(something)


def translate_type(something):
    something = something.lower()
    complex_match = re.match(r'(\w+)(?:\((\d+)\))', something)
    if complex_match:
        _type_name = complex_match.group(1)
        _type_length = int(complex_match.group(2))

        return {
            'varchar': types.String,
            'int': types.Integer,
            'smallint': types.Integer
        }.get(_type_name, None)(*([] if 'int' in _type_name else [_type_length]))
    else:
        return {
            'text': types.Text
        }.get(something)()


__all__ = ['c2k', 'translate_bool', 'translate_type']